# RTSP Web player

This is a proof of concept .NET Core Blazor project for displaying a RTSP
stream in a web page.

## Workflow
- Connect to RTSP stream
- Handle `FrameReceived` event
- Decode frame data to `Bitmap`
- Get base64 string from `BitMap` in JPEG format
- Set Blazor binding to this base64 string so html `img` data base 64 updates
  on the frontend

In the `Data/RtspCameraService.cs` the RTSP stream is configured hard coded for now.

The `Pages/Index.razor` contains the image

```html
<img src="data:image/jpeg;base64,@ImgBase64" width="@Width" height="@Height">
```

The event handler `_rtspCameraService_FrameReceived` updates the `ImgBase64`
binding to the image on the frontend gets updated.

```csharp
private void _rtspCameraService_FrameReceived(object sender, Bitmap bitmap)
{
        using (var memoryStream = new MemoryStream())
        {
            bitmap.Save(memoryStream, ImageFormat.Jpeg);
            ImgBase64 = Convert.ToBase64String(memoryStream.ToArray());
        }

        base.InvokeAsync(StateHasChanged);
        FrameCount++;
}
```
