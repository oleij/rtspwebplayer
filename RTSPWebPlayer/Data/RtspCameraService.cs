﻿using RTSPWebPlayer.RawFramesDecoding;
using RTSPWebPlayer.RawFramesDecoding.DecodedFrames;
using RTSPWebPlayer.RawFramesDecoding.FFmpeg;
using RtspClientSharp;
using RtspClientSharp.RawFrames.Video;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace RTSPWebPlayer.Data
{
    public class RtspCameraService : IDisposable
    {
        bool disposed = false;

        private readonly Dictionary<FFmpegVideoCodecId, FFmpegVideoDecoder> _videoDecodersMap =
            new Dictionary<FFmpegVideoCodecId, FFmpegVideoDecoder>();

        public event EventHandler<Bitmap> FrameReceived;
        public event EventHandler FrameDropped;

        private readonly Mutex _mutex = new Mutex();

        public async Task ConnectAsync(CancellationToken cancellationToken)
        {
            //var serverUri = new Uri("rtsp://192.168.178.16/HDMIIn/stream0");
            //var serverUri = new Uri("rtsp://192.168.178.11:8554/");
            var serverUri = new Uri("rtsp://171.25.232.5/3e3019c02ad14b84bceb2b810db29918");

            var connectionParameters = new ConnectionParameters(serverUri)
            {
                RtpTransport = RtpTransportProtocol.TCP
            };
            using var rtspClient = new RtspClient(connectionParameters);
            rtspClient.FrameReceived += (sender, frame) =>
            {
                if (!(frame is RawVideoFrame rawVideoFrame))
                {
                    return;
                }

                if (_mutex.WaitOne(0))
                {
                    var decoder = GetDecoderForFrame(rawVideoFrame);
                    IDecodedVideoFrame decodedFrame = decoder.TryDecode(rawVideoFrame, out int width, out int height);

                    _mutex.ReleaseMutex();
                    if (width != 0 && height != 0)
                    {
                        using (var bitmap = new Bitmap(width, height))
                        {
                            Rectangle BoundsRect = new Rectangle(0, 0, width, height);
                            var bitMapData = bitmap.LockBits(BoundsRect, ImageLockMode.WriteOnly, bitmap.PixelFormat);
                            IntPtr dataPointer = bitMapData.Scan0;

                            var transformParameters = new TransformParameters(RectangleF.Empty,
                                    new Size(width, height),
                                    ScalingPolicy.Stretch, RawFramesDecoding.PixelFormat.Bgra32, ScalingQuality.FastBilinear);

                            try
                            {
                                decodedFrame.TransformTo(dataPointer, bitMapData.Stride, transformParameters);
                            }
                            finally
                            {
                                bitmap.UnlockBits(bitMapData);
                            }

                            FrameReceived?.Invoke(this, bitmap);
                        }
                    }
                }
                else
                {
                    FrameDropped?.Invoke(this, EventArgs.Empty);
                }
            };

            await rtspClient.ConnectAsync(cancellationToken);
            await rtspClient.ReceiveAsync(cancellationToken);
        }

        private FFmpegVideoDecoder GetDecoderForFrame(RawVideoFrame videoFrame)
        {
            FFmpegVideoCodecId codecId = DetectCodecId(videoFrame);
            if (!_videoDecodersMap.TryGetValue(codecId, out FFmpegVideoDecoder decoder))
            {
                decoder = FFmpegVideoDecoder.CreateDecoder(codecId);
                _videoDecodersMap.Add(codecId, decoder);
            }

            return decoder;
        }

        private FFmpegVideoCodecId DetectCodecId(RawVideoFrame videoFrame)
        {
            if (videoFrame is RawJpegFrame)
                return FFmpegVideoCodecId.MJPEG;
            if (videoFrame is RawH264Frame)
                return FFmpegVideoCodecId.H264;

            throw new ArgumentOutOfRangeException(nameof(videoFrame));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                _mutex.Dispose();
            }

            disposed = true;
        }
    }
}
