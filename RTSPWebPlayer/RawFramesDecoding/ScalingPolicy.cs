﻿namespace RTSPWebPlayer.RawFramesDecoding
{
    public enum ScalingPolicy
    {
        Auto,
        Stretch,
        RespectAspectRatio
    }
}
