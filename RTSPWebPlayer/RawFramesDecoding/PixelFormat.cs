﻿namespace RTSPWebPlayer.RawFramesDecoding
{
    public enum PixelFormat
    {
        Grayscale,
        Bgr24,
        Bgra32,
    }
}